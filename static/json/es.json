{
	"home": {
		"text": {
			"slogan": "continuando la conversación",
			"hdr-news": "Noticias",
			"hdr-dls": "Descargas",
			"hdr-forum": "Foro",

			"s1-h": "Lo que es viejo vuelve a ser nuevo",
			"s1-p": "Escargot es un servidor que permite a viejas versiones de MSN Messenger y Windows Live Messenger funcionar de nuevo.",
			"signup-txt": "Regístrate",
			"dl-msg": "Descarga Messenger",

			"s2-h": "\"¿Pero por qué?\"",
			"s2-p": "<strong>¿Por qué no?</strong> MSN Messenger fue una vez el rey de la mensajería instantánea y los programas de socialización en línea, en una época antes de Facebook, Skype, Discord, o Twitter. Pero entonces Microsoft compró Skype en 2012 y pensó que tener dos servicios de chat separados era redundante, asique MSN fue eliminado. Lo que es una lástima, porque aunque no tenía las más recientes características, igual era una buena plataforma de mensajería instantánea. Ahora varias personas están trabajando en construir con ingeniería inversa un servidor para cada versión importante de MSN, ¡y tú puedes chatear con gente de todo el mundo justo ahora!",

			"s3-h": "\"¿Pero cómo?\"",
			"s3-p": "<strong>Escargot</strong> es un servidor creado desde cero para recrear la arquitectura de los servidores del viejo MSN Messenger, usada por Microsoft cuando todavía los tenía corriendo. El código para recrear el servicio funciona para algunas versiones de MSN, pero (a esta fecha) WLM 2009 y 2011/2012 no funcionan, ya que esas versiones corren con un protocolo distinto y usan una tecnología diferente que tenemos que reprogramar. Sin embargo, todo el código está completamente abierto en <a href='https://gitlab.com/valtron/msn-server'>GitLab</a>, ¡asique siéntete libre de contribuir!",

			"s4-h1": "\"¿Ahora qué?\"",
			"s4-h2": "¡Muchas cosas! Puedes:",
			"s4-signup": "Registrarte en Escargot",
			"s4-dl": "Descargar Messenger",
			"s4-forum": "Hablar en el foro de Escargot",
			"s4-news": "Leer las últimas noticias de Escargot",

			"status-msg1": "1. Requiere la opción \"Soporte para MSN viejo\" activa cuendo crees tu cuenta.<br>\n<strong>Cuidado: Las versiones de MSN 1/2/3/4 no tienen un método de inicio de sesión seguro</strong> y sólo deberías usar \"Soporte para MSN viejo\" con una contraseña desechable.",
			"status-msg2": "2. Sólo las versiones 8.1.0178 y 8.5 están probadas; otras podrían no funcionar.",

			"ftr-home": "Inicio",
			"ftr-signup": "Registrarse",
			"ftr-dls": "Descargas",
			"ftr-news": "Noticias",

			"ftr-txt": "Escargot 2018. Diseño por <a href='http://www.spriteclad.com/main.html' id='Spriteclad-link'>Spriteclad</a>. Sitio web y traducción al español por <a href='http://www.maigol.ml/' id='Maigol-link'>Maigol</a>.\n<br></br>\nMSN, MSN Messenger, Windows Live, \"Windows\", y Windows Live Messenger son propiedades de Microsoft Corporation. Nada de esto nos pertenece. Esto es un proyecto de fans con propósitos personales solamente.",


			"not-found-title": "404 - Página no encontrada",
			"nf-msg1": "El recurso que estabas buscando ha sido renombrado, cambiado de lugar, o no existe.",
			"nf-msg2": "O tal vez escribiste mal la URL.",


			"faq-title": "Preguntas frecuentes",
			"faq-subtitle": "Mayormente basado en <a href='https://wink.messengergeek.com/t/the-escargot-mega-faq-subject-to-change/4849' target='_blank'>este hilo</a>.",

			"q1-s": "¿Necesito una cuenta de Microsoft para usar Escargot?",
			"q1-p1": "Por el momento, la respuesta es no. Esto es porque casi todos los métodos de inicio de sesión que MSN Messenger ha usado a travez de los años básicamente ocultan la contraseña a plena vista, lo que es bueno, pero el acceso a la contraseña le permitiría al servidor enlazar tu cuenta con los servicios existentes de Microsoft, como Outlook, por ejemplo, y enviar notificaciones push de e-mail a usuarios conectados, junto con listas de contactos en tu cuenta de MSN Messenger reflejando los que están en tu libro de direcciónes de email, ya que así funcionaba cuando MSN Messenger estaba activo. Por suerte, dos de los métodos de inicio de sesión de MSN Messenger envian la contraseña en texto plano mediante HTTPS seguro, lo que nos dejaría hacer eso, pero debido a otros conflictos existentes, no podemos utilizar esos para integración de cuentas real hasta que una mejor solución sea encontrada, <a target='_blank' href='https://wink.messengergeek.com/t/msn-7-5-authentication-the-patchening/4549'>lo cual está pasando ahora mismo</a>.",
			"q1-p2": "Sólo sepan que yo recomendaría usar un email real antes que uno inventado, cosa que ya se ha hecho. No solo permitirá que gente tenga la opción de enviar un email cuando estés desconectado, sino que también cuando quieras reestablecer tu contraseña, Escargot intentará enviarte un email con un link para hacerlo. Tener una cuenta de email real atada a tu cuenta de Escargot lo hará posible.",

			"q2-s": "¿Está siendo Escargot desarrollado activamente?",
			"q2-p1": "El desarrollo es esporádico.\nSin embargo, otros frontends (Yahoo! Messenger, IRC, AIM, XMPP) podrían estar de camino. Algunos incluso tienen pruebas de concepto funcionales.",
			"q2-p2": "<a href='https://gitlab.com/valtron/msn-server' target='_blank'>El servidor</a> es enteramente de código abierto y su contribución es bienvenida. Asegúrate de ver la <a href='https://gitlab.com/valtron/msn-server/tree/dev' target='_blank'>rama de desarrollo</a> para el código más reciente.",

			"q3-s": "¿Soportará Escargot otros mensajeros instantáneos actualmente muertos?",
			"q3-p1": "¡Sí! De hecho, un frontend de Yahoo! Messenger está siendo trabajado (y funciona, con algunas fallas). También tenemos planes para otros frontends.",

			"q4-s": "¡No puedo iniciar sesión en Escargot con MSN Messenger 1.0 - 4.7! ¿Por qué?",
			"q4-p1": "Primero, deberías chequear que tu cuenta tenga activada la opción de entrar con esas versiones, ya que usan métdos de autenticación inseguros que eran aceptables en su tiempo, que era autenticación basada en MD5 (el método de autenticación que no nos permite recuperar la contraseña para propósitos de integración para gente usando esas versiones del cliente de MSN). Cuando te estés registrando, activa la casilla de \"Soporte para MSN viejo\", sabiendo que tendrás una entrada de contraseña en la base de datos con tu contraseña, \"hasheada\" con un algoritmo (que ahora es consideraso inseguro) de \"hashing\", antes de terminar el registro de tu cuenta.",
			"q4-p2": "Si ya registraste una cuenta y no has activado la opción, puedes pedir un reestablecimiento de tu contraseña, considerando que no hayas usado un e-mail falso, para activarla. Sin embargo, si no quieres quieres cambiar tu contraseña, puedes tan sólo escribir tu contraseña actual en ambos campos.",
			"q4-p3": "Ahora, respecto a iniciar sesión en los clientes especificados, mientras que hayas parcheado el cliente <strong>CORRECTAMENTE</strong> con <a target='_blank' href='http://storage.log1p.xyz/SetEscargotServer.reg'>el archivo .reg dado en la página de instrucciones de parcheo</a>, y deberías estar hecho. Pero Windows Messenger 4.7.2009 no sigue los valores estándar del rehistro, y sigue la forma de MSN 5.0 - 6.2, utilizando autenticación Tweener en lugar de autenticación basada en MD5 que las versiones anteriores usaban. Esto es porque 4.7.2009 fué una versión modificada de la base de código de la 4.x lanzada alrededor de 2004 que soportaba un método aceptable de autenticación Tweener. Por suerte, no hace falta seguir ninguna instrucción especial para parchear esta versión del cliente, y si tienes confirmado que usas la versión 4.7.2009 (Ve a \"<code>Ayuda</code> -&gt; <code>Acerca de Windows Messenger</code>\", o <code>Acerca de MSN Messenger</code> si se aplican excepciones, mira los números entre parentesis <code>x.x.xxxx</code> para ver que versión usas), Sigue las <a href='/patching'>instrucciónes de parcheo de MSN 5.0 - 6.2</a> y aplícalas a tu copia de Windows Messenger 4.7.2009.",
			"q4-p4": "Ahora, una buena advertencia no sólo de MSN Messenger, sino de cualquier programa:",
			"q4-p5": "<strong>DEBES</strong> usar un editor sexagesimal o un editor de texto que retenga datos binarios y se cuidadoso cuando modifiques los datos de programa, o corromperás el programa de MSN Messenger y/o sus componentes, requiriendo una reinstalación. Si no estás seguro de tus habilidades para modificar los archivos binarios o de qué usar para modificar el programa, consigue un amigo que pueda, o que por lo menos sepa qué hace y déjalo que te ayude con la magia.",

			"q5-s": "Luego de que añadía alguien en mi lista de constactos de WLM 8.x, dicho contacto parece desconectado por un largo tiempo incluso si no lo está. ¿Por qué pasa esto?",
			"q5-p1": "Esto es un bug, y ha sido mayormente arreglado (excepto en casos excepcionales; los \"protocolos\" de WLM 8+'s son un desastre), pero el arreglo no ha sido desplegado al público todavía.",

			"q6-s": "¿Está siendo el soporte para WLM 2009 - 2012 desarrollado? Si no, ¿por qué?",
			"q6-p1": "Sí, al fin. Luego de un año estancados y corriendo en círculos, encontramos por qué WLM 2009 no funcionaba, y era una dependencia faltante separada del MSI de esa versión de WLM.",
			"q6-p2": "Por el momento, el soporte para MSNP18 está casi terminado. Casi todo, excepto Circulos/Grupos y el servicio de \"Qué Hay de Nuevo\" ha sido terminado y probado de que funciona sin problemas, con MPoP siendo implementado, pero parcialmente probado y sin confirmar si funciona como debería. No estamos completamente seguros de implementar \"Qué Hay de Nuevo\", ya que requeriría que Escargot tuviera una red social donde pudieras postear y leer publicaciones del servicio de \"Qué Hay de Nuevo\", y ahora, nos parece mucho, pero de todos modos, esto es un gran logro para nosotros, ya que estamos por terminar el círculo de MSN. Luego sólo falta implementar MSNP21, y se termina. Al menos para los usuarios.",
			"q6-p3": "Pero nosotros, seguiremos retocando todo para hacer MSN lo mejor y más usable posible, asíque seguiremos divirtiendonos jugando con el protocolo de MSN por un rato más.",

			"q7-s": "Escargot me saca y tarda más de lo usual en iniciar sesión o directamente no puedo. ¿Tiene explicación?",
			"q7-p1": "Asumiendo que no hayas tocado los procedimientos de parcheado antes de iniciar sesión, mira <a target='_blank' href='https://wink.messengergeek.com/t/why-the-escargot-server-has-random-malfunction-periods-from-time-to-time/4264'>este hilo</a>.",

			"q8-s": "¿Hay alguna manera de usar MSN Messenger/Escargot en mi celular/telefono móvil?",
			"q8-p1": "Para ustedes quienes tienden a hablar con gente en el camino, había una app oficial de WLM para iOS y unas pocas apps de terceros de MSN Messenger para aquellos usando dispositivos con Android y telefonos basados en Java, pero esos requieren puertas XMPP que todavía no hemos implementado (y posiblemente no tengamos interés en hacerlo actualmente).",
			"q8-p2": "Además, la app de iOS tiene el mismo problema que cualquier otra aplicación de iOS tiene: cirfrado del modulo principal de la app. Hay documentación sobre cómo desencriptar estas bestias, pero están incompletos en términos de utilidades que podamos usar.",
			"q8-p3": "Por suerte, si llegas a tener un dispositivo con Android por ahí, unas personas en el equipo de desarrollo han hechado un ojo a una app llamada Mercury Messenger que se conecta al núcleo de los servidores de MSN/WLM en lugar de depender de puertas, y lo hace bastante bien para ser una app de Android. Ve este hilo para descargar las versiones modificadas de la app que estos miembros del equipo han lanzado para que te conectes a Escargot: <a target='_blank' href='https://wink.messengergeek.com/t/beta-2-mercury-messenger-for-android-with-msnp15-and-msnp11-support/2216'>BETA 2: Mercury Messenger para Android con soporte para MSNP15 y MSNP11</a>. ¡Ahora puedes disfrutar MSN Messenger donde sea, cuando sea! (de nuevo, para aquellos usando Android. Mejor esperen, usuarios de iOS y telefonos de Java.)",

			"q9-s": "Cuando uso Mercury Messenger con la opción \"MSNP11\", no se conecta y se agota el tiempo. ¿Hay alguna manera de arreglarlo?",
			"q9-p1": "Intenta iniciar sesión con la opción <code>MSNP15</code>. Parece que ahora funciona con Mercury Messenger.",

			"q10-s": "¿Escargot implementará anuncios para llenar el hueco para publicidad en MSN/WLM o sólo pondrá anuncios en general?",
			"q10-p1": "Definitivamente, Escargot <strong>NO</strong> insertará anuncios. El departamento legal de Microsoft vería que hacemos dinero con su PI (Propiedad Intelectual; basicamente la manera legal de decir Cosas que le pertenecen a alguien) y nos detendría, destruyendo el proyecto. No queremos eso, ¿o si?",
			"q10-p2": "Aunque podríamos poner anuncios \"de mentiritas\", no para generar ganancias monetarias, si no por diversión o para promocionar proyectos relacionados.",

			"q11-s": "¡Estoy siendo añadido a estos molestos chats grupales/conversaciones multi-usuarios que no puedo bloquear! ¿Puedo hacer algo?",
			"q11-p1": "Esto no es específico de Escargot, y de cualquier forma, esto está fuera de nuestro control, ya que esto es hecho por los propios usuarios. Lo menos que puedes hacer es bloquear a cualquier sospechoso que creas que es el creador de los chats grupales, y si con eso no es suficiente para ti, asegúrate de que en tus opciones de privacidad sólo permitas a gente de tu lista de contactos hablar contigo, como se describe <a target='_blank' href='https://wink.messengergeek.com/t/are-you-being-invited-to-group-chats-over-and-over-come-in-i-have-a-solution/1173'>en este hilo</a>.",

			"q12-s": "¿Se abrirá Escargot un Patreon o empezará a tomar donaciones?",
			"q12-p1": "No hay planes por el momento.",
			"q12-p2": "Las facturas del host son manejables por el momento (la mayor parte del cosoto va dirigido a banda ancha para las descargas), pero si llegamos a hacer algo como esto, probablemente empezaremos tomando donaciones en criptomonedas."
		},
		"status": {
			"supported": "Soportado",
			"wip": "Trabajo en progreso",
			"not-supported": "Todavía no"
		}
	},
	"register": {
		"text": {
			"mkacc-title": "Crea tu cuenta", 
			"mkacc-subtitle": "(Para reestablecer/cambiar la contraseña de una cuenta existente, <a href='/forgot'>haz click aquí</a>.)",
			
			"support_old-text": "Soporte para MSN viejo (necesario para entrar en MSN 1/2/3/4). Estas versiones son <strong>inseguras.</strong>",
			"goto-downloads": "¿Ya tienes una cuenta de Escargot?",
			"mkacc-button": "Crear cuenta",

			"no-errors": "Cuenta <strong id='created_email'>{{ created_email }}</strong> creada. ¡Diviértete!",
			"errors": "Cuenta no creada. Ve los errores debajo."
		},
		"placeholders": {
			"email": "tu@email.com",
			"password1": "6+ caracteres",
			"password2": "Introduce la misma contraseña otra vez"
		},
		"errors": {
			"email": {
				"used": "Este email ya está en uso.",
				"invalid": "Email inválido.",
				"pass-in-email": "No pongas tu contraseña en el campo de email; eso es sólo para cuando inicias sesión en MSN < 5 o 7.5"
			},
			"password1": {
				"too-short": "Contraseña muy corta: 6 caracteres mínimo."
			},
			"password2": {
				"no-match": "Las contraseñas no coinciden."
			}
		}
	},
	"downloads": {
		"text": {
			"step1-title": "Elige un idioma",
			"bg-lng": "Árabe",
			"ar-lng": "Búlgaro",
			"cns-lng": "Chino (Simplificado)",
			"cnt-lng": "Chino (Tradicional)",
			"hr-lng": "Croata",
			"cs-lng": "Checo",
			"da-lng": "Danés",
			"nl-lng": "Neerlandés",
			"en-lng": "Inglés",
			"et-lng": "Estoniano",
			"fi-lng": "Finlandés",
			"fr-lng": "Francés",
			"el-lng": "Griego",
			"he-lng": "Hebreo",
			"hu-lng": "Húngaro",
			"it-lng": "Italiano",
			"ja-lng": "Japón",
			"ko-lng": "Coreano",
			"lv-lng": "Letón",
			"lt-lng": "Lituano",
			"nb-lng": "Noruego",
			"pl-lng": "Polaco",
			"br-lng": "Portugués (Brasil)",
			"pt-lng": "Portugués",
			"ro-lng": "Rumano",
			"ru-lng": "Ruso",
			"sk-lng": "Eslovaco",
			"sl-lng": "Esloveno",
			"es-lng": "Español",
			"sv-lng": "Sueco",
			"th-lng": "Tailandés",
			"tr-lng": "Turco",
			"uk-lng": "Ucraniano",

			"step2-title": "Empieza con Escargot",

			"reco-dl": "DESCARGAR",
			"reco-reco": "versión recomendada",
			"reco-ver": "WLM versión 8.5",
			"reco-size": "12.7 MB - parcheado",

			"ppv-title": "Versiones pre-parcheadas",

			"upv-title": "Versiones sin parchear",
			"patching-link": "(Cómo parchear)",

			"extras-title": "Extras"
		},
		"xtras": {
			"titles": {
				"msgplus3": "Messenger Plus!",
				"msgplus4": "Messenger Plus! Live",
				"msgplussound": "Parche de sonido para Messenger Plus!",
				"msgpluslivesound": "Parche de sonido para Messenger Plus! Live",
				"poly": "MSN Polygamy",
				"emotcreator": "Creador de emoticones",
				"archive": "Archivo de clientes de MSN Messenger",
				"patcher": "Parcheador automático de Escargot",
				"stuffplug30": "StuffPlug 3.0",
				"stuffplug35": "StuffPlug 3.5",
				"richpresence": "Rich presence",
				"mercury": "Mercury Messenger"
			},
			"descs": {
				"msgplus3": "¡Hace MSN más divertido! (MSN 7.5 & abajo) - por Patchou",
				"msgplus4": "¡Hace MSN más divertido! (WLM 8.1 & arriba) - por Patchou",
				"msgplussound": "Cambia el servidor de sonido usado por Messenger Plus! - por Jonathan Kay",
				"msgpluslivesound": "Cambia el servidor de sonido usado por Messenger Plus! Live - por Mateus Rick",
				"poly": "Te permite mantener varios MSNs abiertos al mismo tiempo - por IMMonitor",
				"emotcreator": "Crea emoticones personalizados más facilmente - por Old Bill",
				"archive": "Archivo de todos los instaladores del cliente de MSN - por Enhanox",
				"patcher": "Automaticamente parchea cualquier versión de MSN Messenger sin parchear - por Enhanox",
				"stuffplug30": "Añade características a MSN Messenger",
				"stuffplug35": "Añade características a MSN Messenger",
				"richpresence": "Añade información de Messenger a Discord mediante la característica de Rich Presence - por Craftplacer",
				"mercury": "¡Chatea en el camino! - por Mateus Rick and Tristanleboss"
			}
		}
	},
	"forgot": {
		"text": {
			"forgot-title": "¿Olvidaste tu contraseña?",
			"forgot-subtitle": "Si olvidaste tu contraseña, o sólo quieres cambiarla, escribe tu correo en el campo de debajo.<br />Recibirás un e-mail con instrucciones para cambiar tu contraseña.",

			"send-reset": "Enviar el email de reset",

			"no-errors": "El correo ha sido enviado a <strong id='sent_to'>{{ sent_to }}</strong>.",
			"errors": "El correo no se ha enviado. Vea los errores abajo."
		},
		"errors": {
			"email": {
				"invalid": "Email inválido.",
				"not-registered": "Esta cuenta no existe. ¿No te has <a href='/register'>registrado</a>?",
				"not-sent": "El email no se pudo enviar."
			}
		}
	},
	"reset": {
		"text": {
			"reset-title": "Reestablecer contraseña.",
			"reset-valid": "Usa el formulario de debajo para cambiar tu contraseña.",
			"reset-expired": "Lo sentimos, pero este link ha expirado. Por favor, <a href='/forgot'>intenta de nuevo</a>.",

			"change-passwd": "Cambiar contraseña",

			"no-errors": "Tu contraseña ha sido cambiada.",
			"errors": "Tu contraseña no ha sido cambiada. Ve los errores debajo."
		},
		"errors": {
			"password1": {
				"too-short": "Contraseña muy corta. 6 caracteres mínimo."
			},
			"password2": {
				"no-match": "Las contraseñas no son las mismas."
			}
		}
	},
	"patching": {
		"text": {
			"none-title": "Instrucciones de parcheo",
			"none-body": "Si quieres parchear MSN tú mismo, necesitarás un <a href='https://mh-nexus.de/en/hxd' target='_blank'>editor sexagesimal</a> (pero <a href='https://notepad-plus-plus.org' target='_blank'>Notepad++</a> también funciona si eres cuidadoso), o puedes usar el <a href='http://storage.log1p.xyz/misc/Escargot-Autopatcher-1.2.exe'>parcheador automático para Escargot</a> de <a href='https://gitlab.com/enhanox' target='_blank'>enhanox</a>.",

			"p8-s1": "Reemplaza <code>msidcrl40.dll</code> con <a href='http://storage.log1p.xyz/msidcrl.dll'>este archivo</a>, y edita <code>msnmsgr.exe</code>:",
			"p8-s2": "Cambia <code>messenger.hotmail.com</code> a <code>m1.escargot.log1p.xyz</code>",
			"p8-s3": "Cambia <code>byrdr.omega.contacts.msn.com</code> a <code>ebyrdromegactcsmsn.log1p.xyz</code>",
			"p8-s4": "Cambia <code>tkrdr.storage.msn.com</code> a <code>etkrdrstmsn.log1p.xyz</code>",
			"p8-s5": "Cambia <code>ows.messenger.msn.com</code> a <code>eowsmsgrmsn.log1p.xyz</code>",
			"p8-s6": "Cambia <code>rsi.hotmail.com</code> a <code>ersih.log1p.xyz</code>",
			"p8-s7": "Cambia <code>http://config.messenger.msn.com/Config/MsgrConfig.asmx</code> a <code>https://escargot.log1p.xyz/etc/MsgrConfig?padding=qqqq</code>",

			"p75-s1": "Reemplaza <code>msidcrl.dll</code> con <a href='http://storage.log1p.xyz/msidcrl.dll'>este archivo</a>, y edita <code>msnmsgr.exe</code>:",
			"p75-s2": "Cambia <code>messenger.hotmail.com</code> a <code>m1.escargot.log1p.xyz</code>",
			"p75-s3": "Cambia <code>http://config.messenger.msn.com/Config/MsgrConfig.asmx</code> a <code>https://escargot.log1p.xyz/etc/MsgrConfig?padding=qqqq</code>",

			"p70-s1": "Edita <code>msnmsgr.exe</code>:",
			"p70-s2": "Cambia <code>messenger.hotmail.com</code> a <code>m1.escargot.log1p.xyz</code>",
			"p70-s3": "Cambia <code>https://nexus.passport.com/rdr/pprdr.asp</code> a <code>https://m1.escargot.log1p.xyz/nexus-mock</code>",
			"p70-s4": "Cambia <code>PassportURLs</code> a <code>Passporturls</code>",
			"p70-s5": "Cambia <code>http://config.messenger.msn.com/Config/MsgrConfig.asmx</code> a <code>https://escargot.log1p.xyz/etc/MsgrConfig?padding=qqqq</code>",

			"p50-s1": "Primero edita <code>msnmsgr.exe</code>:",
			"p50-s2": "Cambia <code>messenger.hotmail.com</code> a <code>m1.escargot.log1p.xyz</code>",
			"p50-s3": "Cambia <code>https://nexus.passport.com/rdr/pprdr.asp</code> a <code>https://m1.escargot.log1p.xyz/nexus-mock</code>",
			"p50-s4": "Cambia <code>PassportURLs</code> a <code>Passporturls</code>",
			"p50-s5": "<strong>(Sólo 6.0+)</strong> Cambia <code>http://config.messenger.msn.com/Config/MsgrConfig.asmx</code> a <code>https://escargot.log1p.xyz/etc/MsgrConfig?padding=qqqq</code>",
			"p50-s6": "Luego cambia el valor de la clave de registro <code>HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\MSNMessenger\\Server</code> a <code>m1.escargot.log1p.xyz</code>.",
			"p50-s7": "Puedes usar el editor de registro, o descargar y abrir este archivo: <a href='http://storage.log1p.xyz/SetEscargotServer.reg'>SetEscargotServer.reg</a>",

			"p10-s1": "Luego de instalar, edita el registro y cambia <code>HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\MessengerService\\Server</code> a <code>m1.escargot.log1p.xyz</code>.",
			"p10-s2": "Puedes usar el editor de registro, o descargar y abrir este archivo: <a href='http://storage.log1p.xyz/SetEscargotServer.reg'>SetEscargotServer.reg</a>"
		}
	}
}