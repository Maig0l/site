DB = 'sqlite:///../msn-server/msn.sqlite'
STATS_DB = 'sqlite:///../msn-server/stats.sqlite'
DEBUG = False

# Set these in settings_local to use reCAPTCHA
RECAPTCHA = {
	'api_key': None,
	'secret_key': None,
}

try:
	from settings_local import *
except ImportError:
	raise Exception("Please create settings_local.py")
